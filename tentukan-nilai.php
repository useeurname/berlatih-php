<?php
function tentukan_nilai($number)
{
    echo "<br>";
    if (85 <= $number && $number <= 100) {
        return "Sangat Baik";
    } elseif (70 <= $number && $number < 85) {
        return "Baik";
    } elseif (60 <= $number && $number < 70) {
        return "Cukup";
    } else {
        return "Kurang";
    }
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
