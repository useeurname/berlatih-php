<?php
function ubah_huruf($string)
{
    $temp = "";
    for ($i = 0; $i < strlen($string); $i++) {
        $temp .= chr(ord($string[$i]) + 1);
    }
    echo "<br>";
    return $temp;
}

// TEST CASES
echo ubah_huruf('wow'); // xpx
echo ubah_huruf('developer'); // efwfmpqfs
echo ubah_huruf('laravel'); // mbsbwfm
echo ubah_huruf('keren'); // lfsfo
echo ubah_huruf('semangat'); // tfnbohbu
